package main

import (
	"flag"
	"os"
	"strings"
	"syscall"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

var (
	resourceConfigs string = ""
	resourceName    string = ""
	varPrefix       string = ""
	selectType      string = ""
)

func main() {
	// Parse command-line arguments
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	// flagMasterNetDev := flag.String("master", "", "Master ethernet network device for SRIOV, ex: eth1.")
	flagLogLevel := flag.String("log-level", "info", "Define the logging level: error, info, debug.")
	flagVarPrefix := flag.String("env-var-prefix", defaultVarPrefix, "Define the environment variable prefix: WIDGET.")
	flagWidgetType := flag.String("type-to-select", defaultWidgetType, "Specify which widget type: default is all.")
	flagResourceName := flag.String("resource-name", defaultResourceName, "Define the default resource name: skatelescope.org/widget.")
	flagResourceConfigs := flag.String("resource-configfile", defaultResourceConfigFile, "Specify the resource config file location.")
	flag.Parse()

	switch *flagLogLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	}

	if *flagResourceConfigs != "" {
		resourceConfigs = *flagResourceConfigs
	}
	resourceName = *flagResourceName
	varPrefix = strings.ToUpper(*flagVarPrefix)
	selectType = strings.ToLower(*flagWidgetType)
	log.Debugf("Config file: %s", resourceConfigs)
	log.Debugf("Resource Name an Prefix: %s/%s", resourceName, varPrefix)
	log.Debugf("Type to select: [%s]", selectType)

	log.Infof("Fetching devices.")

	devList, err := GetWidgetDevices(resourceConfigs, selectType)
	if err != nil {
		log.Errorf("Error to get IB device: %v", err)
		return
	}
	if len(devList) == 0 {
		log.Println("No devices found.")
		// return // this is OK - some nodes wont advertise or widgets
	}

	log.Debugf("Widget device list: %v", devList)
	log.Println("Starting FS watcher.")
	watcher, err := newFSWatcher(pluginapi.DevicePluginPath)
	if err != nil {
		log.Println("Failed to created FS watcher.")
		os.Exit(1)
	}
	defer watcher.Close()

	log.Println("Starting OS watcher.")
	sigs := newOSWatcher(syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	restart := true
	var devicePlugin *WidgetDevicePlugin

L:
	for {
		if restart {
			if devicePlugin != nil {
				devicePlugin.Stop()
			}

			devicePlugin = NewWidgetDevicePlugin(resourceConfigs, resourceName, varPrefix, selectType)
			if err := devicePlugin.Serve(resourceName); err != nil {
				log.Println("Could not contact Kubelet, retrying. Did you enable the device plugin feature gate?")
			} else {
				restart = false
			}
		}

		select {
		case event := <-watcher.Events:
			if event.Name == pluginapi.KubeletSocket && event.Op&fsnotify.Create == fsnotify.Create {
				log.Printf("inotify: %s created, restarting.", pluginapi.KubeletSocket)
				restart = true
			}

		case err := <-watcher.Errors:
			log.Printf("inotify: %s", err)

		case s := <-sigs:
			switch s {
			case syscall.SIGHUP:
				log.Println("Received SIGHUP, restarting.")
				restart = true
			default:
				log.Printf("Received signal \"%v\", shutting down.", s)
				devicePlugin.Stop()
				break L
			}
		}
	}
}
