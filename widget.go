package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"

	log "github.com/sirupsen/logrus"
	"gitlab.com/piersharding/k8s-ghost-device-plugin/file"
	"golang.org/x/net/context"
	yaml "gopkg.in/yaml.v2"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

const RdmaDeviceRource = "/sys/class/infiniband/%s/device/resource"
const NetDeviceRource = "/sys/class/net/%s/device/resource"

var flagStrictPerms = flag.Bool("strict.perms", true, "Strict permission checking on config files")

func IsStrictPerms() bool {
	if !*flagStrictPerms || os.Getenv("STRICT_PERMS") == "false" {
		return false
	}
	return true
}

// ownerHasExclusiveWritePerms asserts that the current user or root is the
// owner of the config file and that the config file is (at most) writable by
// the owner or root (e.g. group and other cannot have write access).
func ownerHasExclusiveWritePerms(name string) error {
	if runtime.GOOS == "windows" {
		return nil
	}

	info, err := file.Stat(name)
	if err != nil {
		return err
	}

	euid := os.Geteuid()
	fileUID, _ := info.UID()
	perm := info.Mode().Perm()

	if fileUID != 0 && euid != fileUID {
		return fmt.Errorf(`config file ("%v") must be owned by the beat user `+
			`(uid=%v) or root`, name, euid)
	}

	// Test if group or other have write permissions.
	if perm&0022 > 0 {
		nameAbs, err := filepath.Abs(name)
		if err != nil {
			nameAbs = name
		}
		return fmt.Errorf(`config file ("%v") can only be writable by the `+
			`owner but the permissions are "%v" (to fix the permissions use: `+
			`'chmod go-w %v')`,
			name, perm, nameAbs)
	}

	return nil
}

// widget.devices:
// - type: tpm
//   model: v1
//   connection: 10.10.10.10:10000

type WidgetConfig struct {
	Devices []struct {
		Type       string
		Model      string
		Connection string
		Host       string
	}
}

func LoadFile(path string) (*WidgetConfig, error) {
	if IsStrictPerms() {
		if err := ownerHasExclusiveWritePerms(path); err != nil {
			return nil, err
		}
	}
	var cfg WidgetConfig
	reader, _ := os.Open(path)
	buf, _ := ioutil.ReadAll(reader)
	err := yaml.Unmarshal(buf, &cfg)
	if err != nil {
		log.Errorf("Error parsing YAML config: %v", err)
		return nil, err
	}
	cfgOut, _ := json.Marshal(cfg)
	log.Debugf("loading config from file '%v' => %v", path, string(cfgOut))
	return &cfg, err
}

func GetWidgetDevices(resourceConfigs string, selectType string) ([]Device, error) {

	log.Debugf("Going to read config")

	cfg, err := LoadFile(resourceConfigs)
	if err != nil {
		log.Errorf("Error reading config: %v", err)
		return nil, err
	}

	return generateWidgetDevices(*cfg, selectType)
}

func generateWidgetDevices(widgetConfigs WidgetConfig, selectType string) ([]Device, error) {
	var devs []Device
	// Get all Widget device list
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("Fatal error getting Hostname: %s", err)
		os.Exit(1)
	}
	log.Debugf("Checking config entries against Hostname: %s", hostname)

	for i, w := range widgetConfigs.Devices {
		// check that we have the right host and type
		log.Debugf("Checking %s/%s and %s/%s", hostname, w.Host, selectType, w.Type)
		if (w.Host == "" || w.Host == hostname) && (w.Type == "" || selectType == "" || w.Type == selectType) {
			log.Debugf("Adding device to %s: %s", hostname, w)
			devs = append(devs, Device{
				Id:          i, // starts at 0
				Name:        fmt.Sprintf("%s_%s_%d_%s", w.Type, w.Model, i, w.Connection),
				DeviceType:  w.Type,
				DeviceModel: w.Model,
				Connection:  w.Connection,
			})
		}
	}

	return devs, nil
}

func deviceExists(devs []*pluginapi.Device, id string) bool {
	for _, d := range devs {
		if d.ID == id {
			return true
		}
	}
	return false
}

func watchXIDs(ctx context.Context, devs []*pluginapi.Device, xids chan<- *pluginapi.Device) {
	for {
		select {
		case <-ctx.Done():
			return
		}

		// TODO: check Widget device healthy status
	}
}
