package main

// import (
// 	"gitlab.com/piersharding/k8s-ghost-device-plugin/ibverbs"
// )

type Device struct {
	DeviceModel string
	DeviceType  string
	Id          int
	Name        string
	Connection  string
}
