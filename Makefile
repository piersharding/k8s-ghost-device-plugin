
DEVICE:=k8s-ghost-device-plugin
TAG ?= latest
REPO_PREFIX ?= registry.gitlab.com/piersharding/
RESOURCE ?= skatelescope.org~1tpm
NODE ?= minikube

.DEFAULT: $(DEVICE)

$(DEVICE):
	go build -i -o $(DEVICE) .

build: fmt vet clean $(DEVICE)

all: clean docker

# Run go fmt against code
fmt: ## run fmt
	go fmt ./...

# Run go vet lint checking against code
vet: ## run go vet lint checking against code
	go vet ./...

clean:
	rm -f $(DEVICE) bin/$(DEVICE)

bin/$(DEVICE):
	./build

docker: build bin/$(DEVICE)
	docker build -t $(DEVICE):$(TAG) .

run:
	./bin/$(DEVICE) -h || true

docker_run:
	docker run --rm -ti $(DEVICE):$(TAG) -h || true

push:
	docker tag $(DEVICE):$(TAG) $(REPO_PREFIX)$(DEVICE):$(TAG)
	docker push $(REPO_PREFIX)$(DEVICE):$(TAG)

label:
	kubectl label nodes minikube tpm=true || true

install: label
	kubectl apply -f ghost-device-plugin.yml

delete:
	kubectl delete -f ghost-device-plugin.yml

test:
	kubectl apply -f tpm-test-device.yaml

untest:
	kubectl delete -f tpm-test-device.yaml

logs:
	kubectl -n kube-system logs -l app.kubernetes.io/name=ghost-device-plugin-ds

show:
	# spec.containers[].resources.limits.memory
	kubectl run tpm --image=busybox:1.28.3 --overrides='{"apiVersion":"v1","spec":{"containers":[{"name":"tpm","image": "busybox:1.28.3","command":["sh","-c", "sleep 3600"],"resources":{"limits":{"skatelescope.org/tpm":"1"}}}]}}'
	@echo ""; echo ""
	kubectl get pods tpm -o wide
	@echo ""; echo ""
	kubectl wait --for=condition=ready --timeout=60s pod tpm
	@echo ""; echo ""
	kubectl get pods tpm -o wide
	@echo ""; echo ""
	@echo "** pause **"; read X
	@echo ""; echo ""
	kubectl exec -i tpm -- sh -c 'env' | grep SKA
	@echo "** pause **"; read X
	kubectl delete pods/tpm --force

delete-resource:
	# run 'kubectl proxy &' first
	curl --header "Content-Type: application/json-patch+json" \
	--request PATCH \
	--data '[{"op": "remove", "path": "/status/capacity/$(RESOURCE)"}]' \
	http://localhost:8001/api/v1/nodes/$(NODE)/status
