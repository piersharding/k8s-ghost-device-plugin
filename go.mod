module gitlab.com/piersharding/k8s-ghost-device-plugin

go 1.13

replace k8s.io/kubelet => k8s.io/kubelet v0.19.1

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	google.golang.org/grpc v1.35.0
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/kubelet v0.0.0
)
